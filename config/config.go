package config

import (
	"fmt"
	"io/ioutil"

	yaml "gopkg.in/yaml.v2"
)

type Config struct {
	// 原文目录路径
	SrcPath string `yaml:"src_path"`

	// 生成发布页面的路径
	PubPath string `yaml:"pub_path"`

	BaseURL string `yaml:"base_url"`
}

func Load(filePath string) (*Config, error) {
	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, fmt.Errorf("read file: %w", err)
	}

	var cfg Config
	if err := yaml.Unmarshal(content, &cfg); err != nil {
		return nil, fmt.Errorf("unmarshal yaml: %w", err)
	}

	return &cfg, nil
}
