APP=bin/blog-generator

.PHONY: build
build:
	go build -o $(APP) main.go
