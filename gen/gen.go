package gen

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"path/filepath"
	"sort"
	"sync"

	"gitlab.com/carban14/blog-generator/config"
)

type Generator interface {
	Generate() error
}

func Run(cfg *config.Config) error {
	files, err := ioutil.ReadDir(cfg.SrcPath)
	if err != nil {
		return fmt.Errorf("read src: %w", err)
	}

	var generators []Generator

	postTmplPath := filepath.Join("static", "template.gohtml")
	postTmpl, err := template.ParseFiles(postTmplPath)
	if err != nil {
		return fmt.Errorf("index template: %w", err)
	}

	var posts = make([]*Post, 0, len(files))
	for _, file := range files {
		if !file.IsDir() {
			continue
		}

		post := new(Post)
		postPath := filepath.Join(cfg.SrcPath, file.Name())
		if err := post.Load(postPath); err != nil {
			return fmt.Errorf("load posts: %w", err)
		}

		posts = append(posts, post)

		generators = append(generators, &PostGenerator{
			Post:    post,
			BaseURL: cfg.BaseURL,
			DstDir:  cfg.PubPath,
			Tmpl:    postTmpl,
		})
	}

	homeTmplPath := filepath.Join("static", "home.gohtml")
	homeTmpl, err := template.ParseFiles(homeTmplPath)
	if err != nil {
		return fmt.Errorf("home template: %w", err)
	}

	sort.Sort(ByDateDesc(posts))
	generators = append(generators, &ListGenerator{
		Posts:   posts,
		BaseURL: cfg.BaseURL,
		DstDir:  cfg.PubPath,
		Tmpl:    homeTmpl,
	})

	var wg sync.WaitGroup
	errResult := make(chan error, 1)
	sema := make(chan struct{}, 50)
	for _, g := range generators {
		wg.Add(1)
		go func(g Generator) {
			defer wg.Done()

			sema <- struct{}{}
			defer func() { <-sema }()

			if err := g.Generate(); err != nil {
				errResult <- err
			}
		}(g)
	}

	done := make(chan struct{})
	go func() {
		wg.Wait()
		close(done)
	}()

	select {
	case <-done:
		return nil
	case err := <-errResult:
		if err != nil {
			return err
		}
	}

	return nil
}
