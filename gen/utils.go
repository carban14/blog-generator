package gen

import (
	"fmt"
	"html/template"
	"io"
	"os"
	"strings"
)

func copyFile(src, dst string) (err error) {
	in, err := os.Open(src)
	if err != nil {
		return fmt.Errorf("error reading file %s: %v", src, err)
	}
	defer in.Close()

	out, err := os.Create(dst)
	if err != nil {
		return fmt.Errorf("error creating file %s: %v", dst, err)
	}
	defer func() {
		if e := out.Close(); e != nil {
			err = e
		}
	}()

	if _, err := io.Copy(out, in); err != nil {
		return fmt.Errorf("error writing file %s: %v", dst, err)
	}
	if err := out.Sync(); err != nil {
		return fmt.Errorf("error writing file %s: %v", dst, err)
	}
	return nil
}

type BaseWriter struct {
	Title   string
	URL     string
	Content template.HTML
}

func buildCanonicalLink(path, baseURL string) string {
	parts := strings.Split(path, "/")
	if len(parts) > 2 {
		return fmt.Sprintf("%s/%s/index.html", baseURL, strings.Join(parts[2:], "/"))
	}
	return "/"
}
