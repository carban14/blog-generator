package gen

import (
	"fmt"
	"html/template"
	"os"
	"path/filepath"
)

type ListGenerator struct {
	Posts   []*Post
	DstDir  string
	Tmpl    *template.Template
	BaseURL string
}

type ListWriter struct {
	BaseWriter
	Items []ListItem
}

type ListItem struct {
	Title string
	Date  string
	Link  string
}

func (g *ListGenerator) Generate() error {
	w := ListWriter{
		BaseWriter: BaseWriter{
			Title: "home",
			URL:   g.BaseURL,
		},
	}

	w.Items = make([]ListItem, 0, len(g.Posts))

	for _, post := range g.Posts {
		w.Items = append(w.Items, ListItem{
			Title: post.Meta.Title,
			Date:  post.Meta.Date.String(),
			Link:  fmt.Sprintf("%s/%s/index.html", g.BaseURL, post.Name),
		})
	}

	if err := os.MkdirAll(g.DstDir, 0755); err != nil {
		return fmt.Errorf("create out list dir: %w", err)
	}
	outPath := filepath.Join(g.DstDir, "index.html")
	f, err := os.Create(outPath)
	if err != nil {
		return fmt.Errorf("create out file: %w", err)
	}
	if err := g.Tmpl.Execute(f, w); err != nil {
		return fmt.Errorf("exec template: %w", err)
	}

	return nil
}
