package gen

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	"github.com/russross/blackfriday"
	"gopkg.in/yaml.v2"
)

type PostGenerator struct {
	Post    *Post
	DstDir  string
	Tmpl    *template.Template
	BaseURL string
}

type PostWriter struct {
	BaseWriter
}

func (g *PostGenerator) Generate() error {
	// create dir
	outPostDir := filepath.Join(g.DstDir, g.Post.Name)
	if err := os.MkdirAll(outPostDir, 0755); err != nil {
		return fmt.Errorf("create out post dir: %w", err)
	}

	// copy images
	if imgs := g.Post.Images; len(imgs) != 0 {
		outImgDir := filepath.Join(outPostDir, "images")
		if err := os.MkdirAll(outImgDir, 0755); err != nil {
			return fmt.Errorf("create out img dir: %w", err)
		}

		for _, src := range imgs {
			dst := filepath.Join(outImgDir, filepath.Base(src))
			if err := copyFile(src, dst); err != nil {
				return fmt.Errorf("copy img to out dir: %w", err)
			}
		}
	}

	// exec template
	outPostPath := filepath.Join(outPostDir, "index.html")
	f, err := os.Create(outPostPath)
	if err != nil {
		return fmt.Errorf("create out index: %w", err)
	}
	defer func() { _ = f.Close() }()

	w := PostWriter{
		BaseWriter: BaseWriter{
			Content: template.HTML(g.Post.Content),
			Title:   g.Post.Meta.Title,
			URL:     buildCanonicalLink(outPostDir, g.BaseURL),
		},
	}
	if err := g.Tmpl.Execute(f, w); err != nil {
		return fmt.Errorf("exec template: %w", err)
	}

	return nil
}

type Post struct {
	Name    string
	Meta    Meta
	Content []byte
	Images  []string
}

func (p *Post) Load(dirPath string) error {
	p.Name = filepath.Base(dirPath)

	metaPath := filepath.Join(dirPath, "meta.yaml")
	if err := p.Meta.UnmarshalFile(metaPath); err != nil {
		return fmt.Errorf("meta: %w", err)
	}

	postPath := filepath.Join(dirPath, "index.md")
	mdContent, err := ioutil.ReadFile(postPath)
	if err != nil {
		return fmt.Errorf("read post: %w", err)
	}
	// markdown => html
	htmlContent := blackfriday.MarkdownCommon(mdContent)
	p.Content = htmlContent

	imgDirPath := filepath.Join(dirPath, "images")
	files, err := ioutil.ReadDir(imgDirPath)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}
		return fmt.Errorf("read img dir: %w", err)
	}
	p.Images = make([]string, 0, len(files))
	for _, file := range files {
		imgPath := filepath.Join(imgDirPath, file.Name())
		p.Images = append(p.Images, imgPath)
	}

	return nil
}

type Meta struct {
	Title string
	Date  time.Time
	Tags  []string
}

func (m *Meta) UnmarshalFile(filePath string) error {
	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		return fmt.Errorf("read file: %w", err)
	}

	if err := yaml.Unmarshal(content, m); err != nil {
		return fmt.Errorf("unmarshal: %w", err)
	}

	return nil
}

type ByDateDesc []*Post

func (p ByDateDesc) Len() int           { return len(p) }
func (p ByDateDesc) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
func (p ByDateDesc) Less(i, j int) bool { return p[i].Meta.Date.After(p[j].Meta.Date) }
