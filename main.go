package main

import (
	"gitlab.com/carban14/blog-generator/cmd"
)

func main() {
	cmd.Run()
}
