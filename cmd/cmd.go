package cmd

import (
	"flag"
	"log"

	"gitlab.com/carban14/blog-generator/config"
	"gitlab.com/carban14/blog-generator/gen"
)

var (
	cfgPath string
)

func init() {
	flag.StringVar(&cfgPath, "c", "config.yaml", "config file path")
	flag.Parse()
}

func Run() {
	cfg, err := config.Load(cfgPath)
	if err != nil {
		log.Fatal(err)
	}

	if err := gen.Run(cfg); err != nil {
		log.Fatal(err)
	}
}
